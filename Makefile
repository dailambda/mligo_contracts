LIGO=../_build/install/default/bin/ligo
# LIGO=../docker/ligo
# LIGO=docker run -v `pwd`:/home/tezzy/work -w /home/tezzy/work -it -t dailambda/ligo:2019-08 /home/tezzy/ligo

.SUFFIX:
.SUFFIX: mligo tz

MODULES=unit \
	boomerang \
	bug_tz \
	manager \
	assert \
	multisig \
	vote \
	address \
	\
	literals \
	arithmetics \
	bools \
	comparisons \
	conditional \
	tuple \
	strings \
	sets \
	maps \
        records \
	variants \
	lists \
	toplevel \
	local \
	optional \
	type_annotations \
	pattern_matching \
	transactions \
	assert \
	crypto \
	bytes \
	\
	balance \
	unsupported

TARGETS=$(addprefix _build/,$(addsuffix .tz,$(MODULES)))

all:  $(TARGETS)

_build/%.tz: %.mligo
	if [ ! -d _build ]; then mkdir _build; fi
	LIGO_HACK=y $(LIGO) compile-contract $< main
	./dryrun.sh $<
	mv $(<:.mligo=.tz) _build

clean:
	rm -rf _build

