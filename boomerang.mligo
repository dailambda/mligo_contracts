let main (_param : unit) (_storage: unit) =
  let x = amount in (* amount is for AMOUNT *)
  let ops = ([] : operation list) in
  let ops = if x = 0tz then ops else Operation.transaction () amount (Operation.get_contract source : unit contract) :: ops in
  ( ops, () )

