let main (_param : unit) (_storage: tez) =
  ( ([]: operation list), 1tz + 0.1tz)

(* There was a bug around parsing and printing of tz values.
   
   * 1tz is parsed as 1mutez.
   * 1mutez is printed as 1tz in dry-run

   Both are fixed in dailambda-dev.
*)
   
(*  If operation is misspelled, operaiton is reported as a type variable.

Error: in file "", line 0, characters 0-0. unbound type variable:  {"variable":"operaiton","in":"- E[_param -> unit , _storage -> tez , arguments -> tuple[tez , unit]]\tT[] ]"}
*)
