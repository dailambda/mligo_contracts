# Camligo cheat sheet

This is based on a modified version of LIGO available at https://gitlab.com/dailambda/ligolang

## Basic literals

&nbsp;          | Examples                | Note
----------------|------------------------|---------
String          | `"Hello"`              | The type is `string`. UTF-8 is not supported.
Integer         | `123`, <br> `-32`           | The type is `int`.
Natural numbers | `34p`                  | The type is `nat`.
Tez             | `1.23tz`               | The type is `tez`.
Boolean         | `true`, <br> `false`        | The type is `bool`.
Unit            | `()`                   | The type is `unit`.

## Arithmetics

Note that not all numeric type combinations are allowed:

Arithmetics     | Examples                | Note
----------------|------------------------|------------
Addition	    | `3 + 4`, <br> `3p + 4p`, <br> `1.2tz + 3.4tz`
Subtraction     | `4 - 3`, <br> `2p - 4p`, <br> `50tz - 23.5tz`  | Subtraction of 2 `nat`s has type `int`.
Multiplication  | `3 * 4`, <br> `3p * 4p`, <br> `1.2tz * 2p`, <br> `3p * 0.1tz`
Division        | `10 / 5`, <br> `10p / 5p`, <br> `2.0tz / 3p`
Modulo	        | `10 mod 3`
Nat to int      | `int 3p`
Absolute value  | `abs (-3)`

<!--
Negation        | NOT SUPPORTED
-->

## Boolean operations

Bool ops.       | Examples
----------------|------------------------
And             | `true && false`
Or              | `true `<code>&#124;&#124;</code>` false`
Negation        | `not true`

## Comparisons

The return type is `bool`:

Comparisons   | Examples
--------------|------------------
Equal         | `1 = 2`
Non-equal     | `"hello" <> "world"`
Less-than, etc.  | `1 < 2`, <br> `1 > 2`, <br> `1 <= 2`, <br> `1 >= 2`


## Conditional: "if-then-else"

The conditional is functional: `else` clause cannot be omitted.

Conditional | Examples
------------|--------------------------------
if-then-else | `if x < 0 then "N" else "ZP"`

## Anonymous functions: "Lambdas"

| Examples
|---------------------------
| `fun (x : int) -> x + 1`

Note: arguments must be type annotated.

## Strings

Strings | Examples
----------|---------------
Literal   | `"hello"`
Length    | `String.length "hello"`
Substring | `String.sub 0p 4p "hello" = "hell"`, <br> `String.sub 1p 4p "hello" = "ello"`
Concatenation | `String.concat "hello " "world"`

Note: No UTF-8 support yet.

## Lists

Lists | Examples
----------|---------------
Construction | `([] : int list)`, <br> `[1 ; 2 ; 3]`
Deconstruction | Use pattern match:<br> `match xs with`<br><code>&#124;</code>` [] -> false `<br><code>&#124;</code>` x::xs -> false`
Length    | `List.length [ 1 ; 2 ; 3 ]`
Map | `List.map [ 1 ; 2 ; 3 ] (fun (x : int) -> x + 1)`

Notes

* `[]` requires a type annotation.
* Map/fold are not supported.

## Sets

Set operations are function: they return new sets,
instead of modifying the given sets:

Sets | Examples
-----|----------------
Construction | `(Set [] : int set)`, <br> `Set [ 1 ; 2 ; 3 ]`
Membership   | `Set.mem 1 set`
Addition     | `Set.add 4 set`
Removal      | `Set.remove 3 set`

Notes

* The type of set is `ty set`, ex. `int set`, `bool set`
* `Set []` requires a type annotation.
* Map/fold are not supported.

## Maps: "Key value pairs"

Map operations are function: they return new maps,
instead of modifying the given maps:

Sets | Examples
-----|----------------
Construction | `(Map [ ] : (string, int) map)`, <br> `Map [ ("Alice", 1) ; ("Bob", 2) ; ("Charlie", 3) ]`
Query        | `Map.find_opt "Alice" m`
Query (fail if not found)  | `Map.find "Angela" m`
Insertion    | `Map.add "Daniel" 4 m`
Update       | `Map.update "Ethan" (Some 5) m` for update, <br> `Map.update "Charlie" (None : int option) m` for removal
Removal      | `Map.remove "Alice" m`

Notes

* The type of set is `(key_ty, val_ty) map`, ex. `(string, int) map`
* `Map []` requires a type annotation.
* `Map.add` can override existing bindings
* `Map.update key (None : ty) m` requires a type annotation around `None`.
* Map/fold are not supported.

## Tuples: "Records without names"

Tuples          | Examples               | Note
----------------|-----------------------|---------------------------
Construction |  `(1, "hello")`       | The type is `ty * ty'`
Field access | `tuple.(0)`,<br> `tuple.(1)`

Note: Use `t.(i)` to access fields, instead of pattern match.

## Records: "Tuples with named fields"

Records         | Examples
----------------|-----------------------
Type definition  |  `type t = { name : string ; age : nat }`
Construction     | `{ name = "Mike" ; age = 42p }`
Field access     | `record.name`,<br> `record.age`

## Optionals: "Some or none"

Optionals | Examples
----------|---------------
Construction | `Some 1`, <br> `(None : int option)`
Deconstruction | Use pattern match:<br> `match x with`<br><code>&#124;</code>` Some y -> y `<br><code>&#124;</code>` None -> 0`

Note: `None` requires a type annotation

## Variants: "This is A or B, or C"

Variants | Examples      
---------|--------------
Type definition  | `type t = Foo of int `<code>&#124;</code>` Bar of string * tez`
Construction |    `Foo 1`,<br> `Bar ("hello", 10tz)`
Deconstruction | Use pattern match: <br> `match v with Foo x -> x * 1tez `<code>&#124;</code>` Bar (s, tz) -> tz`

Note: 0-ary constructor is not supported.
Use `type t = Constr of unit` instead of `type t = Constr`

## Pattern matching

Pattern match is to branch over `option`s and variants:

```
match opt with
| None -> 0tz
| Some tez -> tez
```

```
type t = Foo of unit | Bar of int * int

match Bar (1, 1) with
| Foo _ -> 0
| Bar ii -> ii.(0) + ii.(1)
```

Notes

* No nested patterns allowed.
* The constractor in pattern must have either a variable or a wild card `_`.

## Toplevel definitions

Toplevel definitions | Examples           | Note
---------------------|-------------------|----------
Value definition    | `let x = 1`
Function definition | `let f (x : int) (y : int) = x + y` | The args must be with types.

Note: `let` can take either a variable or a wild card `_`.

## Local definitions

`let var = expr in ..`

Local definitions  | Examples 
-------------------|-----------------------
Values  | `let quad (x : int) =`<br> &nbsp;&nbsp;`let x2 = x + x in` <br> &nbsp;&nbsp; `x2 + x2`
Funcitons | `let quad' (x : int) =`<br> &nbsp;&nbsp; `let double = fun (x : int) -> x + x in` <br>&nbsp;&nbsp; `double (double x)`
Ignoring result | `let _ = assert (3 * 4 = 12) in ...`

Notes:

* `let` can only take either a variable or a wild card `_`.
* Local let cannot take the form `let f (x:ty) = ... in ...`.
  Use lambda abstraction `fun` instead: `let f = fun (x:ty) -> ... in ...`.

## Type annotations

`None`, `[]`, and arguments of function definitions and `fun` require type annotations:

| Examples
|--------------------
| `let f (x : int) = x + 1`
| `fun (x : int) -> x + 1`
| `( None : int option )`
| `([] : operation list)`
| `("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address)`

## Assertions: "Test then fail if something wrong"

`assert b` fails the execution of the contract if `b` is evaluated to `false`:

```
let main (param : int) (storage : int) =
  (* Fail if the parameter is different from the storage. *)
  let _ = assert (param = storage) in
  (([] : operation list), ()) 
```

## Environments: "Global variables":

Name    | Type | Notes
----------------|-------|------------------------------
`balance`       | `tez` | The current amount of tez in the current contract.
`time`          | `timestamp` | The timestamp at the validation.
`amount`        | `tez` | The amount of tez of the transaction.
`sender`        | `address` | The contract which called the current contract.
`source`        | `address` | The contract which initiated the whole transaction.
<!-- self -->

## Transactions: "Contract related operations"

&nbsp;          | Examples                 | Notes
----------------|--------------------------|------
Address         | `("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" : address)`,<br> `("KT1JepfBfMSqkQyf9B1ndvURghGsSB8YCLMD" : address)`  | Type annotation required.
Contract        | `(Operation.get_contract address : unit contract)` | Type annotation required.
Transaction     | `Operation.transaction () 3tz cntr`  | The result type is `operation`.

Notes:

* Contract construction requires a type annotation to declare the contract's return type.
* `Operation.transaction` takes the following arguments:
    * Parameter to call the contract.
	* The amount of token to send.
	* The target contract.
  The types of the contract argument and the parameter must agree.
	
* `operation` values must be returned by the entry function to be executed by the blockchain.

## Bytes: "Binary representations"

Bytes | Examples
----------|---------------
Packig    | `Bytes.pack (12, "hello", 3tz)`
Unpack    | `(Bytes.unpack bs : (int * string * tez) option)`
Length    | `Bytes.length bs`
Subbytes  | `Bytes.sub 0p 4p bs`
Concatenation | `Bytes.concat bs1 bs2`

Notes

* `Bytes.pack` can pack any value to its binary representation.
* The packed value by `Bytes.pack` can be recovered by `Bytes.unpack` with a type annotation.
  The result is an `option` therefore needs to use a pattern matching to examine it.

