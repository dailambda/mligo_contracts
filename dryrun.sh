#!/bin/sh
if head $1 | grep TESTABLE; then
    LIGO_HACK=y ../_build/install/default/bin/ligo dry-run $1 main '()' '()'
fi

