(* TESTABLE *)
let main (param : unit) (storage : unit) =
  let x = Some (Some 1) in
  let _ =
    match x with
    | None -> assert false
    | Some x -> 
       (match x with
        | None -> assert false
        | Some x -> ())
  in
  ( ([] : operation list), () )
    
