## Error locations

They are widely broken. Lots of `generated` and `file "", line 0, characters 0-0`, which have no useful information.  
Seems to be hard to fix, since AST elements are not attached with locations.

**Partially fixed**: 
Introduced a very dirty hack to show the not-pin-pointed but most detailed error locations.

## `failwith` and `assert`

### `failwith` is unusable.

The type of `failwith` is monomorphic, which prohibits from writing:

```
if b then failwith "error" else (([] : operation list), ())
```

The following workaround does *not* work either:

```
let _unit = if b then failwith "error" else () in ...
```

this is rejectged by Michelson's type checker.

Current workaround is to use `assert`: 

```
let _unit = assert b in ...
```

### `assert` is monomorphic

Same as `failwith`, `assert` is monomorphic.  This makes hard to write a "`Some` test".

The following code is impossible:

```
match Map.find_opt k m with
| Some v -> v
| None -> assert false
```

We have to write as follows instead, using a dummy value:

```
match Map.find_opt k m with
| Some v -> v
| None -> 
    let _ = assert false in
	some_dummy_value
```

## `SELF` does not work

`self` was added but it does not work:

```
let%entry main (storage : unit) (param : unit) =
  let c = self in (* Proto alpha rejects this code due to "proto.alpha.selfInLambda" *)
  ( ([] : operation list), ())
```

Michelson type system somehow rejects the code saying the `SELF` is in a lambda.
This prevents from implementing the multisig contract https://gitlab.com/nomadic-labs/mi-cho-coq/blob/master/src/contracts/arthur/multisig.tz in Ligodity.

## `LIST_ITER` does not work

I have wired `LIST_ITER` to Ligodity's `List.iter` but it did not work.

Ligodity's function of type `'elt -> unit` is coverted to a Michelson code of type `'elt : 'A -> unit : 'A`,
which does not match with the type for the `ITER`'s body: `'elt : 'A -> 'A`.

Meanwhile, we can use `List.map` instead.


## No way to pattern match `list`

Implemented.  (I hope)

## `%entry` does not force the return type of the function

It is checked later by Michelson but LIGO should check it.

## Nullary variant contstructor cannot be defined

`type t = A` does not work.

## `LIST_MAP` argument order

The order is flipped (in OCaml's sense), though.

## Local function is definable only via `fun (x : ty) ->`

```
let ...
  let f (x : int) = ... in  (* impossible.  Must write f = fun (x : int) -> *)
  ...
```

## Nested `match` parse failure

The following should be parsed, but failed:

```
match x with
| None -> assert false
| Some x ->
	match x with
	| None -> assert false
	| Some x -> ()
```

## Missing features

```
<data> ::=
  | <int constant>
  | <natural number constant>
  | <string constant>                No UTF-8 strings
  | <timestamp string constant>      No literals
  | <signature string constant>      No literals
  | <key string constant>            No literals
  | <key_hash string constant>       No literals
  | <mutez string constant>
  | <contract string constant>       No literals
  | Unit
  | True
  | False
  | Pair <data> <data>
  | Left <data>
  | Right <data>
  | Some <data>
  | None
  | { <data> ; ... }
  | { Elt <data> <data> ; ... }
  | instruction
<instruction> ::=
  | { <instruction> ... }
  | DROP
  | DUP
  | SWAP
  | PUSH <type> <data>
  | SOME
  | NONE <type>
  | UNIT
  | IF_NONE { <instruction> ... } { <instruction> ... }
  | PAIR
  | CAR
  | CDR
  | LEFT <type>
  | RIGHT <type>
  | IF_LEFT { <instruction> ... } { <instruction> ... }
  | IF_RIGHT { <instruction> ... } { <instruction> ... }
  | NIL <type>
  | CONS
  | IF_CONS { <instruction> ... } { <instruction> ... }
  | SIZE
  | EMPTY_SET <comparable type>
  | EMPTY_MAP <comparable type> <type>
  | MAP { <instruction> ... }
  | ITER { <instruction> ... }                             No folding on list, set, map
  | MEM
  | GET
  | UPDATE
  | IF { <instruction> ... } { <instruction> ... }
  | LOOP { <instruction> ... }
  | LOOP_LEFT { <instruction> ... }
  | LAMBDA <type> <type> { <instruction> ... }
  | EXEC
  | DIP { <instruction> ... }
  | FAILWITH <data>                                        failwith is not working
  | CAST                                                   No way to convert between tez and nat ?
  | RENAME
  | CONCAT
  | SLICE
  | PACK
  | UNPACK
  | ADD
  | SUB
  | MUL
  | EDIV
  | ABS
  | NEG                                                    Unary minus is not implemented
  | LSL                                                    Not supported
  | LSR                                                    Not supported
  | OR
  | AND
  | XOR                                                    No logical xor binop
  | NOT
  | COMPARE                                                Bools are not comparable
  | EQ
  | NEQ
  | LT
  | GT
  | LE
  | GE
  | SELF                                                   Not working
  | CONTRACT <type>
  | TRANSFER_TOKENS
  | SET_DELEGATE                                           Not implemented
  | CREATE_ACCOUNT                                         Not implemented
  | CREATE_CONTRACT { <instruction> ... }                  Not implemented
  | IMPLICIT_ACCOUNT
  | NOW
  | AMOUNT
  | BALANCE
  | CHECK_SIGNATURE
  | BLAKE2B                                                Not implemented
  | SHA256
  | SHA512
  | HASH_KEY                                               Not implemented
  | STEPS_TO_QUOTA                                         Not implemented
  | SOURCE
  | SENDER
  | ADDRESS
<type> ::=
  | <comparable type>
  | key                                                    No literals
  | unit
  | signature                                              No literals
  | option <type>
  | list <type>
  | set <comparable type>
  | operation
  | contract <type>
  | pair <type> <type>
  | or <type> <type>
  | lambda <type> <type>
  | map <comparable type> <type>
  | big_map <comparable type> <type>                       Not implemented
<comparable type> ::=
  | int
  | nat
  | string                                                 No UTF-8
  | bytes
  | mutez
  | bool
  | key_hash                                               No literal
  | timestamp                                              No literal
  | address
```

