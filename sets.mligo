(* TESTABLE *)
let main (param : unit) (storage : unit) =
  let s = (Set [ ] : int set) in
  let s = Set [ 1 ; 2 ; 3 ] in
  let _ = assert (Set.mem 1 s && not (Set.mem 4 s)) in
  let s = Set.add 4 s in
  let s = Set.remove 3 s in
  let _ = assert (not (Set.mem 3 s)) in
  let _ = assert (Set.mem 4 s) in

  ( ([] : operation list), () )

