(*
../_build/default/src/bin/cli.exe dry-run --syntax cameligo taco-shop/taco-shop-01.mligo main 3 4
*)

let%entry main (parameter : int) (contract_storage : int) : (operation list * int) =
  (([] : operation list), contract_storage + parameter)
