(*
../_build/default/src/bin/cli.exe dry-run --syntax cameligo taco-shop/taco-shop-02.mligo main unit 'Map[(1p, {current_stock = 50p; max_price = 50000000tz}); (2p, {current_stock = 20p; max_price = 75000000tz})]'
*)

type taco_supply = {
  current_stock : nat;
  max_price : tez;
}

type taco_shop_storage = (nat, taco_supply) map

let%entry main (parameter : unit) (taco_shop_storage : taco_shop_storage) :
  (operation list * taco_shop_storage) =
  (([] : operation list), taco_shop_storage)
