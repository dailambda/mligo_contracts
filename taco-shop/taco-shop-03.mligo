(*
../_build/default/src/bin/cli.exe dry-run --syntax cameligo taco-shop/taco-shop-03.mligo buy_taco 1p 'Map[(1p, {current_stock = 50p; max_price = 50000000tz}); (2p, {current_stock = 20p; max_price = 75000000tz})]'
*)

type taco_supply = {
  current_stock : nat;
  max_price : tez;
}

type taco_shop_storage = (nat, taco_supply) map

let%entry buy_taco (taco_kind_index : nat) (taco_shop_storage : taco_shop_storage) :
  (operation list * taco_shop_storage) =
  let taco_kind = Map.find taco_kind_index taco_shop_storage in
  let taco_kind' =
    {current_stock = abs (taco_kind.current_stock - 1p);
     max_price = taco_kind.max_price}
  in
  let taco_shop_storage' =
    Map.update taco_kind_index (Some taco_kind') taco_shop_storage
  in
  (([] : operation list), taco_shop_storage')
