(* TESTABLE *)
let main (param : unit) (storage : unit) =
  let tuple = (1, "hello") in

  let i = tuple.(0) + 2 in (* getting the first (0th) element, which is int *)

  let hello = tuple.(1) in (* getting the first (1th) element, which is string *)

  ( ([] : operation list), assert (i = 3 && hello = "hello" ) )
    
