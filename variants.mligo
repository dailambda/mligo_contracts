(* TESTABLE *)
type t = 
  | Nothing of unit 
  | Send of tez
              
let main (param : unit) (storage : unit) =
  let x = Nothing () in
  let y = Send 10tz in

  (* let _ = assert (x = x) in *) (* variants are not comparable *)
  
  let _ = 
    assert (match x with
            | Nothing _ -> true
            | Send _ -> false)
  in

  let _ = 
    assert (match y with
            | Nothing _ -> false
            | Send tez -> tez = 10tz)
  in

  ( ([] : operation list), ())    
